# NetBeans remote PHPUnit runner #

Custom PHPUnit script that allows NetBeans to run tests on a remote host. Developed mainly to make working with Docker environments more convenient.

## Installation ##

You can simply place the [remote-phpunit.sh](src/remote-phpunit.sh) script wherever you want to on your local computer.

Alternatively, you can install this [composer](https://getcomposer.org/) package as a requirement of your test stack.

```bash
composer require interitty/nb-remote-phpunit
```

## Usage ##

Activate remote execution in [NetBeans](https://netbeans.org/) as follows:

- Right-click your project and click "`Properties`".
- Navigate to "`Testing > PHPUnit`".
- Check "`Use Custom PHPUnit Script`" and point "`PHPUnit Script`" to `SCRIPT_PATH/remote-phpunit.sh --docker-container=phpunit_1`, where `SCRIPT_PATH` is an absolute path to the folder, where is the [remote-phpunit.sh](src/remote-phpunit.sh) script situated.
- Run tests as usual.

When the script is a part of the project test stack, the `SCRIPT_PATH` will be `PROJECT_PATH/vendor/bin/`.

Mostly default docker container name `phpunit_1` is not the one you use. Don't forget to change this attribute.

![NetBeans configuration example](assets/netbeans-configuration-example.png)

## Settings ##

There are some more settings, you may need to make to suit your needs.

| Script parameter             | Description                                                                                                                                                                                                                                                                          |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `--base-dir="…"`             | The base directory of all paths, which is also used for truncating output messages.                                                                                                                                                                                                  |
| `--bootstrap "…"`            | The path to the `bootstrap.php` file. The default value is empty. Should be filled from NetBeans project settings "`Use Bootstrap > Bootstrap`".                                                                                                                                     |
| `--colors`, `--colors="…"`   | Use colors in output ("`never`", "`auto`" or "`always`"). The default value is "`auto`". Can be changed by the argument of the script.                                                                                                                                               |
| `--configuration "…"`        | Path to the `phpunit.xml` file. By default, the `phpunit.xml` file is searched in the current working directory or parent folders. Can be filled from NetBeans project settings "`Use XML Configuration > XML Configuration`".                                                       |
| `--coverage-clover "…"`      | Local path of the coverage report file. The default value is empty. Should be filled automatically, when the NetBeans asks for a [coverage report](https://netbeans.org/kb/docs/php/phpunit.html#code-coverage).                                                                     |
| `--coverage-text`            | Generate code coverage report usable in the CI/CD pipelines                                                                                                                                                                                                                          |
| `--docker-container="…"`     | Name of the container, where should be remote PHPUnit executed. Default value is "`phpunit_1`". Can be changed by the argument of the script.                                                                                                                                        |
| `--docker-path="…"`          | The path for the local docker binary. Default value is "`/usr/local/bin/docker`". Can be changed by the argument of the script.                                                                                                                                                      |
| `--fail-fast`                | Stop processing other tests when the first error happens.                                                                                                                                                                                                                            |
| `--filter="…"`               | Name of the specific test to test. Should be filled automatically, when the NetBeans [run a focused test method](https://blogs.oracle.com/geertjan/run-focused-test-method).                                                                                                         |
| `--group "…"`                | Comma-separated list of test groups to test. The default value is empty. Should be filled automatically, when the NetBeans test [selected groups](https://netbeans.org/kb/docs/php/phpunit.html#test-groups).                                                                        |
| `--list-groups`              | When present, the list of available test groups is printed. Should be filled automatically, when the NetBeans asks for [test groups](https://netbeans.org/kb/docs/php/phpunit.html#test-groups).                                                                                     |
| `--log-junit "…"`            | Local path of the PHPUnit result file from where NetBeans parse the result. Should be filled automatically by the NetBeans.                                                                                                                                                          |
| `--preserve-suite`           | By default, the script skips sending `NetBeansSuite.php` to the docker because it is difficult to get him there and is useless. If it is intentionally required, use this parameter.                                                                                                 |
| `--remote-coverage-path="…"` | Remote path of the coverage report file. The parent folder of the path should be in the [docker volume](https://docs.docker.com/storage/volumes/). The default value is "`/tmp/junit/nb-phpunit-coverage.xml`".                                                                      |
| `--remote-junit-path="…"`    | Remote path of the PHPUnit result file from where NetBeans parse the result. The parent folder of the path should be in the [docker volume](https://docs.docker.com/storage/volumes/). The default value is "`/tmp/junit/nb-phpunit-log.xml`".                                       |
| `--remote-phpunit-path="…"`  | Remote path of the PHPUnit binary. Default value is "`/root/.composer/vendor/bin/phpunit`". Can be changed by the argument of the script.                                                                                                                                            |
| `--run="…"`                  | Path of the file or folder of tests the PHPUnit runs on. Should be filled in automatically by the NetBeans.                                                                                                                                                                          |
| `-v`, `--verbose`            | Show debugging information about executed commands and filled variables. Can be set by the argument of the script.                                                                                                                                                                   |
| `--whitelist="…"`            | Path of the files for checking the code coverage. The default value is empty. Should be filled automatically by the argument of the script. More info in [PhpUnit docs](https://phpunit.de/manual/current/en/code-coverage-analysis.html#code-coverage-analysis.whitelisting-files). |

## Environment variables ##

Some settings can also be set using an environment variable.

| Environment variable | Description                                             |
| -------------------- | ------------------------------------------------------- |
| `CLICOLOR=1`         | Enable color output.                                    |
| `DEBUG=1`            | Increase verbosity level to show also debug messages.   |
| `VERBOSE=1`          | Increase verbosity level to show also warning messages. |

## Limitations ##

This script requires the local path to the project to be the same as the path in the docker container. It does not provide any path conversion. The `/opt/srv` (*[opt](https://tldp.org/LDP/Linux-Filesystem-Hierarchy/html/opt.html), [srv](https://www.tldp.org/LDP/Linux-Filesystem-Hierarchy/html/srv.html)*) folder is perfectly suited for this purpose.
