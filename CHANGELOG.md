# Changelog #
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] ##

### Added ###

### Changed ###

### Fixed ###

## [1.0.4] - 2024-12-07 ##

### Changed ###

- Upgrade license to 2025

## [1.0.3] - 2024-05-19 ##

### Changed ###

- Update SECURITY key

## [1.0.2] - 2023-12-28 ##

### Changed ###

- Update security contacts
- Upgrade license to 2024

## [1.0.1] - 2022-12-29 ##

### Changed ###

- Upgrade license to 2023

## [1.0.0] - 2022-07-22 ##

### Added ###

- Initial release of NetBeans remote PHPUnit runner
