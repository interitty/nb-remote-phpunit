#!/bin/bash

# Script variables
ARGUMENTS=""
BASE_DIR="${PWD}"
BIN_DIR=""
BOOTSTRAP=""
COLORS="NO"
CONFIGURATION=""
COVERAGE_CLOVER=""
COVERAGE_TEXT="NO"
DOCKER_PATH=""
EXIT_CODE=0
FAIL_FAST="NO"
LIST_GROUPS="NO"
REMOTE_COVERAGE_PATH="/tmp/junit/nb-phpunit-coverage.xml"
REMOTE_JUNIT_PATH="/tmp/junit/nb-phpunit-log.xml"
REMOTE_PHPUNIT_PATH="/root/.composer/vendor/bin/phpunit"
REMOTE_WORKDIR="${PWD}"
SKIP_SUITE="YES"
RUN=""
VERBOSITY_LEVEL=1

# Print given debug message
function debug() {
    local MESSAGE="${1}"

    if [ ${VERBOSITY_LEVEL} -gt 2 ]; then
        echo -e "${COLOR_WHITE}DEBUG${COLOR_NORMAL} - ${MESSAGE}\n"
    fi
}

# Try to find the bin-dir location
function detect_bin_dir() {
    local LOCAL_BIN_DIR="$(real_path $(PATH="${PATH}:/usr/local/bin" command -v docker))"

    if [ -z "${LOCAL_BIN_DIR}" ]; then
        error "Unable to find Docker bin directory"
        exit ${EXIT_CODE}
    fi
    echo "$(dirname ${LOCAL_BIN_DIR})"
}

# Execute given command via docker
function docker_exec() {
    local COMMAND="${1}"
    local DOCKER_ARGUMENTS=""

    if [ "$(is_docker_running ${DOCKER_DEV_CONTAINER})" == "n" ]; then
        error "Docker container \"${DOCKER_DEV_CONTAINER}\" is not running"
    else
        if [ -z "${DOCKER_PATH}" ]; then
            DOCKER_PATH="${BIN_DIR}/docker"
        fi

        DOCKER_ARGUMENTS+=" exec"
        DOCKER_ARGUMENTS+=" --workdir $(real_path ${BASE_DIR})"
        if [ "${COLORS}" == "YES" ]; then
            DOCKER_ARGUMENTS+=" --env CLICOLOR=1"
        fi
        DOCKER_ARGUMENTS+=" -t ${DOCKER_DEV_CONTAINER}"
        DOCKER_ARGUMENTS+=" ${COMMAND}"

        exec "${DOCKER_PATH}${DOCKER_ARGUMENTS}"
    fi
}

# Print given error message
function error() {
    local MESSAGE="${1}"

    if [ ${VERBOSITY_LEVEL} -gt 0 ]; then
        >&2 echo -e "${COLOR_RED}ERROR${COLOR_NORMAL} - ${MESSAGE}\n"
    fi
    set_exit_code 1
}

# Print given command and execute them
function exec() {
    local COMMAND="${1}"
    local OUTPUT=""
    local PROFILE=""
    if [ -e "~/.bash_profile" ]; then
        PROFILE="\". ~/.bash_profile &>/dev/null && "
    fi

    debug "exec ${COMMAND}"

    if [ ${VERBOSITY_LEVEL} -gt 0 ]; then
        bash -c "${PROFILE}${COMMAND}"
    else
        OUTPUT=$(bash -c "${PROFILE}${COMMAND}")
    fi
    set_exit_code $?
}

# Use for find specified file from FIND_PATH to UP
function find_up() {
    local FIND_PATH="${1}"
    local FILE="${2}"

    while [[ "${FIND_PATH}" != "" && ! -e "${FIND_PATH}/${FILE}" ]]; do
        FIND_PATH=${FIND_PATH%/*}
    done
    if [ -n "${FIND_PATH}" ]; then
      echo -e "${FIND_PATH}/${FILE}"
    fi
}

# Check if docker is running
function is_docker_running() {
    local DOCKER_CONTAINER_NAME="${1}"
    local DOCKER_RUNNING="n"
    local DOCKER_STATUS=$(${BIN_DIR}/docker ps --filter name=${DOCKER_CONTAINER_NAME} --format "{{.Names}}|{{.Status}}" | sed "s+${DOCKER_CONTAINER_NAME}|++g")

    if [ "${DOCKER_STATUS:0:2}" == "Up" ]; then
        DOCKER_RUNNING="y"
    fi

    echo ${DOCKER_RUNNING}
}

# Convert symlink path to real path
function read_link() {
    local LINK="${1}"
    local READLINK_COMMAND="$(PATH="${PATH}:/usr/local/bin" command -v greadlink)"

    if [ -z "${READLINK_COMMAND}" ]; then
        READLINK_COMMAND="$(PATH="${PATH}:/usr/local/bin" command -v readlink)"
    fi
    echo $(${READLINK_COMMAND} -f ${LINK})
}

# Retreive real absolute path from given relative path
function real_path() {
    local RELATIVE_PATH="${1}"
    local REAL_PATH=""

    if [ -n "${RELATIVE_PATH}" ]; then
        if [ "${RELATIVE_PATH}" == "." ] || [ "${RELATIVE_PATH}" == "./" ]; then
            REAL_PATH="${BASE_DIR}"
        elif [ "${RELATIVE_PATH}" == ".." ] || [ "${RELATIVE_PATH}" == "../" ]; then
            REAL_PATH="$(dirname ${BASE_DIR})"
        else
            REAL_PATH="$(dirname $(read_link ${RELATIVE_PATH}))/$(basename ${RELATIVE_PATH})"
        fi
        if ! [ -e "${REAL_PATH}" ]; then
            REAL_PATH=""
        fi
    fi
    echo ${REAL_PATH}
}

# Set global exit code
function set_exit_code() {
    local CURRENT_EXIT_CODE="${1}"

    if [ ${CURRENT_EXIT_CODE} -ne 0 ]; then
        if [ "${FAIL_FAST}" == "YES" ]; then
            exit ${CURRENT_EXIT_CODE}
        elif [ ${EXIT_CODE} -eq 0 ] || [ ${CURRENT_EXIT_CODE} -lt ${EXIT_CODE} ]; then
            EXIT_CODE="${CURRENT_EXIT_CODE}"
        fi
    fi
}

# Set color codes
function setup_colors() {
    if [ "${COLORS}" == "YES" ]; then
        COLOR_GREEN="\033[0;32m"
        COLOR_NORMAL="\033[0;00m"
        COLOR_RED="\033[0;31m"
        COLOR_WHITE="\033[0;37m"
        COLOR_YELLOW="\033[1;33m"
    else
        COLOR_GREEN=""
        COLOR_NORMAL=""
        COLOR_RED=""
        COLOR_WHITE=""
        COLOR_YELLOW=""
    fi
}

# Grab all paths for check
function setup_paths() {
    BIN_DIR="$(detect_bin_dir)"
}

# Print given success message
function success() {
    local MESSAGE="${1}"

    if [ ${VERBOSITY_LEVEL} -gt 0 ]; then
        echo -e "${COLOR_GREEN}SUCCESS${COLOR_NORMAL} - ${MESSAGE}\n"
    fi
}

# Dump usage help message
function usage() {
cat << EOF

USAGE: remote-phpunit.sh [OPTIONS]

    Run given <COMMAND> in the docker container where the name is set in
    the DOCKER_DEV_CONTAINER environment variable if is running.

OPTIONS:

    --base-dir="…"               The base directory of all paths, which is also used for truncating output messages
    --bootstrap "…"              The path to the bootstrap.php file. Default value is empty. Should be filled from NetBeans project settings "Use Bootstrap > Bootstrap"
    --colors, --colors="…"       Use colors in output ("never", "auto" or "always"). Default value is "auto". Can be changed by the argument of script
    --configuration "…"          Path to the phpunit.xml file. At default the phpunit.xml file is searched in the current working directory or parent folders. Can be filled from NetBeans project settings "Use XML Configuration > XML Configuration"
    --coverage-clover "…"        Local path of the coverage report file. Default value is empty. Should be filled automatically, when the NetBeans asks for coverage report
    --coverage-text              Generate code coverage report usable in the CI/CD pipelines
    --docker-container="…"       Name of the container, where should be remote PHPUnit executed. Default value is "phpunit_1". Can be changed by the argument of script
    --docker-path="…"            The path for the local docker binary. Default value is "/usr/local/bin/docker". Can be changed by the argument of script
    --fail-fast                  Stop processing another tests, when the first error happen
    --filter="…"                 Name of the specific test to test. Should be filled automatically, when the NetBeans run focused test method
    --group "…"                  Comma separated list of test groups to test. Default value is empty. Should be filled automatically, when the NetBeans test selected groups
    -h, --help                   Show this help
    --list-groups                When present, the list of available test groups is printed. Should be filled automatically, when the NetBeans asks for test groups
    --log-junit "…"              Local path of the PHPUnit result file from where NetBeans parse the result. Should be filled automatically by the NetBeans
    --preserve-suite             By default, the script skips sending "NetBeansSuite.php" to the docker because it is difficult to get him there and is useless. If it is intentionally required, use this parameter.
    --remote-coverage-path="…"   Remote path of the coverage report file. Parent folder of the path should be in the docker volume. Default value is "/tmp/junit/nb-phpunit-coverage.xml"
    --remote-junit-path="…"      Remote path of the PHPUnit result file from where NetBeans parse the result. Parent folder of the path should be in the docker volume. Default value is "/tmp/junit/nb-phpunit-log.xml"
    --remote-phpunit-path="…"    Remote path of the PHPUnit binary. Default value is "/root/.composer/vendor/bin/phpunit". Can be changed by the argument of script
    --run="…"                    Path of the file or folder of tests the PHPUnit run on. Should be filled in automatically by the NetBeans
    -v, --verbose                Show debuging information about executed commands and filled variables. Can be setted by the argument of script
    --whitelist="…"              Path of the files for checking the code coverage. Default value is empty. Should be filled automatically by the by the argument of script. More info in PhpUnit docs

EOF
exit 0
}

# Print given warning message
function warning() {
    local MESSAGE="${1}"

    if [ ${VERBOSITY_LEVEL} -gt 1 ]; then
        >&2 echo -e "${COLOR_YELLOW}WARNING${COLOR_NORMAL} - ${MESSAGE}\n"
    fi
    set_exit_code 2
}

# Setup color output by global environment
if [ "${CLICOLOR}" == "1" ]; then
    COLORS="always"
    setup_colors
fi

# Setup default docker dev container name
if [ -z "${DOCKER_DEV_CONTAINER}" ]; then
    DOCKER_DEV_CONTAINER="phpunit_1"
fi

# Setup verbosity level by global environment
if [ "${DEBUG}" == "1" ]; then
    VERBOSITY_LEVEL=3
elif [ "${VERBOSE}" == "1" ]; then
    VERBOSITY_LEVEL=2
fi

# Parse arguments
POSITIONAL=()
ARGUMENTS=$@
while [[ $# -gt 0 ]]
do
    key="${1}"
    case $key in
        --base-dir=*)
            BASE_DIR="$(real_path ${key#*=})"
            shift
        ;;
        --bootstrap)
            BOOTSTRAP="${2}"
            shift
            shift
        ;;
        --colors)
            COLORS="YES"
            setup_colors
            shift
        ;;
        --colors=*)
            if [ "${key#*=}" == "always" ]; then
                COLORS="YES"
            elif [ "${key#*=}" == "never" ]; then
                COLORS="NO"
            fi
            setup_colors
            shift
        ;;
        --configuration)
            CONFIGURATION="${2}"
            shift
            shift
        ;;
        --coverage-clover)
            COVERAGE_CLOVER="${2}"
            shift
            shift
        ;;
        --coverage-text)
            COVERAGE_TEXT="YES"
            shift
        ;;
        -vv|--debug)
            if [ ${VERBOSITY_LEVEL} -eq 1 ]; then
                VERBOSITY_LEVEL=3
            fi
            shift
        ;;
        --docker-container=*)
            DOCKER_DEV_CONTAINER="${key#*=}"
            shift
        ;;
        --docker-path=*)
            DOCKER_PATH="${key#*=}"
            shift
        ;;
        --fail-fast)
            FAIL_FAST="YES"
            shift
        ;;
        --filter)
            FILTER="${2//\\b/}"
            shift
            shift
        ;;
        --group)
            GROUP="${2}"
            shift
            shift
        ;;
        -h|--help)
            HELP="YES"
            shift
        ;;
        --list-groups)
            LIST_GROUPS="YES"
            shift
        ;;
        --log-junit)
            LOG_JUNIT="${2}"
            shift
            shift
        ;;
        --preserve-suite)
            SKIP_SUITE="NO"
            shift
        ;;
        --remote-coverage-path=*)
            REMOTE_COVERAGE_PATH="${key#*=}"
            shift
        ;;
        --remote-junit-path=*)
            REMOTE_JUNIT_PATH="${key#*=}"
            shift
        ;;
        --remote-phpunit-path=*)
            REMOTE_PHPUNIT_PATH="${key#*=}"
            shift
        ;;
        --remote-workdir=*)
            REMOTE_WORKDIR="${key#*=}"
            shift
        ;;
        --run=*)
            RUN="${key#*=}"
            shift
        ;;
        -v|--verbose)
            if [ ${VERBOSITY_LEVEL} -eq 1 ]; then
                VERBOSITY_LEVEL=2
            fi
            shift
        ;;
        --whitelist=*)
            WHITELIST="${key#*=}"
            shift
        ;;
        *)
            # unknown option
            if [[ ${#POSITIONAL} -eq 0 ]] && [ "${1:0:1}" == "-" ]; then
                warning "Unsupported parameter \"${1}\""
                shift
            else
                POSITIONAL+=("${1}") # save it in an array for later
                shift # past argument
            fi
        ;;
    esac
done
set -- "${POSITIONAL[*]}" # restore positional parameters

# Main section of script
if [ "${HELP}" == "YES" ]; then
    usage
fi

setup_paths

if [ -z "${DOCKER_DEV_CONTAINER}" ]; then
    error "Global environment \"DOCKER_DEV_CONTAINER\" is not set"
    exit ${EXIT_CODE}
fi

if [ -z "${CONFIGURATION}" ]; then
    CONFIGURATION=$(find_up "${BASE_DIR}" "phpunit.xml")
fi

PHPUNIT_ARGUMENTS=""
if [ "${COLORS}" == "YES" ]; then
    PHPUNIT_ARGUMENTS+=" --colors=always"
fi
if [ -n "${BOOTSTRAP}" ]; then
    PHPUNIT_ARGUMENTS+=" --bootstrap \"${BOOTSTRAP}\""
fi
if [ -n "${CONFIGURATION}" ]; then
    PHPUNIT_ARGUMENTS+=" --configuration \"${CONFIGURATION}\""
fi
if [ -n "${FILTER}" ]; then
    PHPUNIT_ARGUMENTS+=" --filter \"${FILTER}\""
fi
if [ -n "${LOG_JUNIT}" ]; then
    PHPUNIT_ARGUMENTS+=" --log-junit \"${REMOTE_JUNIT_PATH}\""
fi
if [ -n "${COVERAGE_CLOVER}" ]; then
    PHPUNIT_ARGUMENTS+=" --coverage-clover \"${REMOTE_COVERAGE_PATH}\""
fi
if [ "${COVERAGE_TEXT}" == "yes" ]; then
    PHPUNIT_ARGUMENTS+=" --coverage-text"
fi
if [ "${LIST_GROUPS}" == "YES" ]; then
    PHPUNIT_ARGUMENTS+=" --list-groups"
else
    if [ -n "${GROUP}" ]; then
        PHPUNIT_ARGUMENTS+=" --group \"${GROUP}\""
    fi
    if [ -n "${RUN}" ]; then
        PHPUNIT_ARGUMENTS+=" \"${RUN}\""
    fi
fi
if [ -n "${1}" ]; then
    if [ "${SKIP_SUITE}" == "NO" ] || [ "${1: -17}" != "NetBeansSuite.php" ]; then
        PHPUNIT_ARGUMENTS+=" \"${1}\""
    fi
fi

docker_exec "${REMOTE_PHPUNIT_PATH}${PHPUNIT_ARGUMENTS}"

if [ -n "${LOG_JUNIT}" ]; then
    docker_exec "cat ${REMOTE_JUNIT_PATH} > ${LOG_JUNIT}"
fi
if [ -n "${COVERAGE_CLOVER}" ]; then
    docker_exec "cat ${REMOTE_COVERAGE_PATH} > ${COVERAGE_CLOVER}"
fi

exit ${EXIT_CODE}